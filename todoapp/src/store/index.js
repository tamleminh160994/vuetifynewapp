import Vue from "vue";
import Vuex from "vuex";
import * as tasks from "./modules/Tasks.js";
import * as snackbarForEditting from "./modules/SnackbarForEditting.js";
import * as Pagination from "./modules/Pagination.js";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        tasks,
        snackbarForEditting,
        Pagination,
    },
});