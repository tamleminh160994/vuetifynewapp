import axios from "axios";

export const state = {
    tasks: [],
    dateOfTask: {},
    totalTasks: {},
};
export const mutations = {
    LOAD_INITIALTASKS(state, { initialTasks, totalTasks }) {
        state.tasks = initialTasks;
        state.totalTasks = totalTasks;
    },
    UPDATE_SELECTEDSTATUSTASK(state, updatedTask) {
        state.tasks = state.tasks.map((task) => {
            const item = updatedTask === task;
            return item ? item : task;
        });
    },
    ADD_NEWTASK(state, newTask) {
        state.tasks.push(newTask);
    },
    UPDATE_TASKTITLE(state, updateTaskTitle) {
        state.tasks = state.tasks.map((task) => {
            const item = updateTaskTitle === task;
            return item ? item : task;
        });
    },
    SORT_ALLTASKSBYTHETITLE(state) {
        function compare_to_sort(x, y) {
            if (x.title < y.title) return -1;
            if (x.title > y.title) return 1;
            return 0;
        }
        state.tasks = state.tasks.sort(compare_to_sort);
    },
    DELETE_SELECTEDTASK(state, id) {
        state.tasks = state.tasks.filter((task) => task.id !== id);
    },
    SHOW_DATEOFTASK(state, { id, seletedFormatDate }) {
        state.dateOfTask = { id, seletedFormatDate };
    },
    FILTERTASKBYTITLE(state, titleFilterTasks) {
        state.tasks = state.tasks.filter((task) => {
            return task.title.toLowerCase().startsWith(titleFilterTasks);
        });
    },
};
export const actions = {
    loadInitialTasks({ commit }, { perPage, page }) {
        axios
            .get("http://localhost:3000/tasks?_limit=" + perPage + "&_page=" + page)
            .then((response) => {
                const totalTasks = response.headers["x-total-count"];
                const initialTasks = response.data;
                commit("LOAD_INITIALTASKS", { initialTasks, totalTasks });
            })
            .catch((error) => {
                console.log(error);
            });
    },
    updateStatusTask({ commit, state }, id) {
        const updateTask = state.tasks.find((task) => task.id === id);
        updateTask.done = !updateTask.done;
        axios
            .put(`http://localhost:3000/tasks/${id}`, updateTask)
            .then((response) => {
                commit("UPDATE_SELECTEDSTATUSTASK", response.data);
            })
            .catch((error) => {
                console.log(error);
            });
    },
    addNewTask({ commit }, newTask) {
        axios
            .post("http://localhost:3000/tasks", newTask)
            .then((response) => {
                commit("ADD_NEWTASK", response.data);
            })
            .catch((error) => {
                console.log(error);
            });
    },
    updateSelectedTaskTitle({ commit, state }, { id, newEditTitle }) {
        const updateTaskTitle = state.tasks.find((task) => task.id === id);
        updateTaskTitle.title = newEditTitle;
        axios
            .put(`http://localhost:3000/tasks/${id}`, updateTaskTitle)
            .then((response) => {
                commit("UPDATE_TASKTITLE", response.data);
            })
            .catch((error) => {
                console.log(error);
            });
    },
    sortAllTasksByTheTitle({ commit }) {
        commit("SORT_ALLTASKSBYTHETITLE");
    },
    deleteSelectedTask({ commit }, id) {
        axios
            .delete(`http://localhost:3000/tasks/${id}`)
            .then(() => {
                commit("DELETE_SELECTEDTASK", id);
            })
            .catch((error) => {
                console.log(error);
            });
    },
    showDateOfSelectedTask({ commit }, { id, seletedFormatDate }) {
        commit("SHOW_DATEOFTASK", { id, seletedFormatDate });
    },
    filterTasksByTitle({ commit }, titleFilterTasks) {
        commit("FILTERTASKBYTITLE", titleFilterTasks);
    },
};
export const getters = {};