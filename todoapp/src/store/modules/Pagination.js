export const state = {
    page: {},
    
};
export const mutations = {
    PAGENUMBEROFTASKS(state, page) {
        state.page = page;
    },
};
export const actions = {
    pageNumberTasks({ commit }, page) {
        commit("PAGENUMBEROFTASKS", page);
    },
};
export const getters = {};