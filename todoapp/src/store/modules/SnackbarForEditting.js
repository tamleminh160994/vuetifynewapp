export const state = {
    snackbarObject: {},
};
export const mutations = {
    SET_INITIALSTATUSSNACKBAR(state, { showDialog, titleAction }) {
        state.snackbarObject = { showDialog, titleAction };
    },
    SHOW_SNACKBARFOREDITTING(state) {
        state.snackbarObject.showDialog = true;
        state.snackbarObject.titleAction = "Task has been editted!!!";
    },
    SHOW_SNACKBARFORSORTING(state) {
        state.snackbarObject.showDialog = true;
        state.snackbarObject.titleAction = "Tasks have been sorted!!!";
    },
    SHOW_SNACKBARFORDELETING(state) {
        state.snackbarObject.showDialog = true;
        state.snackbarObject.titleAction = "The selected Task has been deleted!!!";
    },
};
export const actions = {
    showSnackbarForEditting({ commit }) {
        commit("SHOW_SNACKBARFOREDITTING");
    },
    initialStatusSnackbar({ commit }, { showDialog, titleAction }) {
        commit("SET_INITIALSTATUSSNACKBAR", { showDialog, titleAction });
    },
    showSnacbarForSorting({ commit }) {
        commit("SHOW_SNACKBARFORSORTING");
    },
    showSnackbarForDeleting({ commit }) {
        commit("SHOW_SNACKBARFORDELETING");
    },
};
export const getters = {};